##############################################################################################

from bs4 import BeautifulSoup
import requests

url = 'https://westernmass.craigslist.org/search/apa'
content = BeautifulSoup(requests.get(url).content, 'html.parser')

for listing in content.find_all('li', {'class': 'result-row'}):
    title = listing.find('p').find('a').text
    price = listing.find('span', {'class': 'result-price'}).text
    link = listing.find('a')['href']

    print(title[:30])
    print(price)
    print(link.replace('https://westernmass.craigslist.org/apa/d/', '') + '\n')

##############################################################################################

import bs4, requests

url = 'https://westernmass.craigslist.org/search/apa'
soup = bs4.BeautifulSoup(requests.get(url).content, 'html.parser')

all_elements = [
    (listing.find('p').find('a').text, listing.find('span', {'class': 'result-price'}).text, listing.find('a')['href'])
    for listing in soup.find_all('li', {'class': 'result-row'})]

for every_listing in all_elements:
    for info in every_listing:
        print(info)
    print()

##############################################################################################

import bs4, requests

soup = bs4.BeautifulSoup(requests.get('https://westernmass.craigslist.org/search/apa').content, 'html.parser')

print_everything = lambda x: [print(i) for i in x]

for listing in soup.find_all('li', {'class': 'result-row'}):
    print_everything((listing.find('p').find('a').text, listing.find('span', {'class': 'result-price'}).text,
                      listing.find('a')['href'], '\n'))

##############################################################################################

import bs4, requests

print_everything, url = lambda x: [print(i) for i in x], 'https://westernmass.craigslist.org/search/apa'

[print_everything((listing.find('p').find('a').text, listing.find('span', {'class': 'result-price'}).text,
                   listing.find('a')['href'], '\n')) for listing in
 bs4.BeautifulSoup(requests.get(url).content, 'html.parser').find_all('li', {'class': 'result-row'})]

##############################################################################################

import bs4, requests

print_everything, url = lambda x: [print(i) for i in x], 'https://westernmass.craigslist.org/search/apa'

[print_everything((listing.find('p').find('a').text[:30], listing.find('span', {'class': 'result-price'}).text,
                   listing.find('a')['href'].replace('https://westernmass.craigslist.org/apa/d/', ''), '\n')) for
 listing in bs4.BeautifulSoup(requests.get(url).content, 'html.parser').find_all('li', {'class': 'result-row'})]

##############################################################################################

import requests, bs4

content = bs4.BeautifulSoup(requests.get('https://westernmass.craigslist.org/search/apa').content, 'html.parser')

for listing in content.find_all('li', {'class': 'result-row'}):
    title, price, link = listing.find('p').find('a').text[:30], listing.find('span', {'class': 'result-price'}).text, \
                         listing.find('a')['href'].replace('https://westernmass.craigslist.org/apa/d/', '') + '\n'

    print(title + '\n', price + '\n', link + '\n')

##############################################################################################
